#pragma once
#include "Mothers.h"

class Node
{
    //Atributos de la clase Pasajero
public:
    Node* Next; //Nodo puntero al siguiente nodo creado
    Mothers Value; //Atributo de que cada valor es un pasajero

public:
    Node(const Mothers& m); //Constructor de la clase Nodo
};

