#include "PriorityDiagnosis.h"
#include "Node.h"
#include "Mothers.h"

//Implementamos el constructor de Cola de Prioridad inicializamos la cabeza
PriorityDiagnosis::PriorityDiagnosis() :Head(nullptr)
{
}

//Implementamos el m�todo booleano de lista vac�a que retorna true or false.
bool PriorityDiagnosis::EmptyQueue()
{
    //Si el frente de la pila est� apuntando a nulo est� vaci�.
    return (Head == nullptr) ? true : false;
}

//Implementamos el m�todo de insertar a la cola
void PriorityDiagnosis::InsertQueue(const Mothers& mothers)
{
    Node* nuevoNodo = new Node(mothers); //Creamos en memoria los nuevos nodos de pasajero
    if (EmptyQueue()) { //Verificamos que la lista cola est� vac�a
        Head = nuevoNodo; //S�, la cabeza es el nuevo nodo  
    }

    else if (nuevoNodo->Value.GetPriority() > Head->Value.GetPriority()) {
        //siguienute del nuevo nodo debe ser el que estaba antes en la cabaza
        nuevoNodo->Next = Head;
        // Cabeza tiene que convertirse en nuevo nodo
        Head = nuevoNodo;

    }
    else {
        Node* nodoActual = Head;
        while (nodoActual->Next != nullptr && nodoActual->Next->Value.GetPriority() >= nuevoNodo->Value.GetPriority()) {
            /*Primero verificamos que el nodo actual apuntando al siguiente sea distinto de nullptr
            lurgo hacemos que el actual nodo que apunta al siguiente con el valor de prioridad sea mayor o igual al
            nuevo nodo con la prioridad y los compara, esa debe de ser la condici�n del while.*/
            nodoActual = nodoActual->Next;
            /*El nodoActual va ser el nodoActual apuntando al siguiente
            esto hace avanzar cada nodo hasta el �ltimo de la lista*/
        }
        /*luego el nuevoNodo apuntando al siguiente va ser el nodoActual apuntando al siguiente para
        que el nuevo nodo tenga el nodoActual siguiente de �l*/
        nuevoNodo->Next = nodoActual->Next;
        /*Luego el nodoActual apuntando al siguiente va ser el nuevo nodo para colocar el nuevo nodo
        entre el nodo Actual luego el nuevo nodo y despu�s el nodoActual del siguiente*/
        nodoActual->Next = nuevoNodo;
    }
}

Mothers PriorityDiagnosis::BringOut()
{
    if (EmptyQueue()) //Si la lista es vacia
        throw EmptyListException;  //Implica que hay un error
    Mothers resultado = Head->Value; //La variable local de resultado guarda la Cabeza apuntando al valor.
    Head = Head->Next; //La cabeza va ser la Cabeza apuntando al siguiente para ir por cada pasajero existente.
    return resultado; //Retornamos el resultado.
}