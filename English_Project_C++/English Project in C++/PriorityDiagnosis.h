#pragma once
#include "Node.h"

class ListaVaciaException :public std::exception {
};

class PriorityDiagnosis
{
public:
    Node* Head; //El nodo creado es la nueva cabeza. 

public:
    PriorityDiagnosis(); //Constructor de la clase ColaPrioridad
    bool EmptyQueue(); //M�todo de ListaVacia
    //M�todo de insertar un nuevo pasajero a la cola
    void InsertQueue(const Mothers& pasajero);
    Mothers BringOut(); //M�todo de sacar a los pasajeros de la cola
    Mothers EmptyListException; //M�todo de lista vac�a
};