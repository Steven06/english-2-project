#include "Mothers.h"

Mothers::Mothers()
{
}

Mothers::Mothers(int severity, int symtoms)
{
    Severity = severity;
    Symptoms = symtoms;
}

int Mothers::GetPriority()
{
    //return Edad;

    if (Symptoms == 100 && Severity == 2) { //S� es mayor a 100 a�os y es mujer 
        return Symptoms;                      //Su prioridad es de 1.
    }
    else if (Symptoms == 65 && Severity == 1) { //S� la edad es igual a 65 a�os y es hombre
        return Symptoms;                             //Su prioridad es de 1.
    }
    else if (Symptoms == 40 && Severity == 3) { //S� la edad es igual a 40 a�os y es hombre
        return Symptoms;                             //Su prioridad es de 2.
    }
    else if (Symptoms == 55 && Severity == 1) { //S� la edad es igual a 55 a�os y es mujer
        return Symptoms;                             //Su prioridad es de 2.
    }
    else if (Symptoms == 24 && Severity == 4) { //S� la edad es igual a 24 a�os y es mujer
        return Symptoms;                             //Su prioridad es de 2.
    }
    else if (Symptoms == 15 && Severity == 3) { //S� la edad es igual a 15 a�os y es hombre
        return Symptoms;                             //Su prioridad es de 3.
    }
    else if (Symptoms == 18 && Severity == 2) { //S� la edad es igual a 18 a�os y es mujer
        return Symptoms;                              //Su prioridad es de 3.
    }
    else if (Symptoms == 23 && Severity == 1) { //S� la edad es igual a 23 a�os y es hombre
        return Symptoms;                             //Su prioridad es de 2.
    }
    else {           //S� es menor a 40 a�os y es hombre o mujer.
        return Symptoms;    //Su prioridad es de 3
    }
}

//M�todos de la clase de Pasajero.
bool Mothers::getSeverity()
{
    return Severity;
}

void Mothers::SetSeverity(int severity)
{
    Severity = severity;
}

int Mothers::getSymtoms()
{
    return Symptoms;
}

void Mothers::SetSymtoms(int symptoms)
{
    Symptoms = symptoms;
}