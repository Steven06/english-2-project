#include "Menu.h"

int Menu::menu()
{

    bool run = true;
    while (run != false)
    {
        std::string text;
        std::cout << std::endl << "----------------------------" << std::endl;

        std::cout << std::endl << "      Main Menu   " << std::endl;
        std::cout << std::endl << "----------------------------" << std::endl;
        std::cout << "1. Insert\n";
        std::cout << "2. Display\n";
        std::cout << "3. Quit\n";
        std::cout << "Enter your choice : ";

        std::getline(std::cin, text);

        if (text == "1")
        {

            int item;
            int priority;
            std::cout << "Enter its priority : ";
            std::cin >> priority;
            std::cout << "Input the item value to be added in the queue : ";
            std::cin >> item;
            Mothers mother(priority, item);
            m.InsertQueue(mother);
            return menu();

        }
        
        if (text == "2")
        {

            Mothers PrintMothers = m.BringOut();
            std::cout << PrintMothers.GetPriority() << std::endl;
            return menu();

        }

        if(text == "3")
        {

            std::cout << "End of Program.\n";
            run = false;
            continue;
       
        }
    }

    return 0;

}
